{**
* 2007-2019 PrestaShop and Contributors
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/AFL-3.0
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to https://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com>
  * @copyright 2007-2019 PrestaShop SA and Contributors
  * @license https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
  * International Registered Trademark & Property of PrestaShop SA
  *}

  <div class="block_newsletter col-lg-8 col-md-12 col-sm-12">
    <div class="row">
      <!--<p id="block-newsletter-label" class="col-md-5 col-xs-12">{l s='Get our latest news and special sales' d='Shop.Theme.Global'}</p>-->
      <div class="">
        <form action="{$urls.pages.index}#footer" method="post">
          <div class="row">
            <div class="col-xs-12" style="text-align:center;">
              <img src="./img/newsletter.png" style="width: 100%;">
              <div class="block_content" style="padding: 10px !important;border-radius: 5px;background-color: #d9d6d1;border: 1px solid #B7B7B7;padding-bottom: 0px;text-align: center;margin-left: 12px;margin-top: -6px;margin-right: 12px;">
                <p><input type="text" style="width: 122px;height:23px;min-width: 100px;" name="email" size="18"
                    value="votre e-mail" onfocus="javascript:if(this.value=='votre e-mail')this.value='';"
                    onblur="javascript:if(this.value=='')this.value='votre e-mail';"></p>
                <p>
                  <input type="submit"
                    style="font-size: 11px;font-weight: bold;text-transform: uppercase;width: 80px;border: 1px solid #b7b7b7;background-color: #f1f1f1;border-radius: 5px;height: 24px;"
                    value="ok" class="buttomsub" name="submitNewsletter">
                </p>

                <div style="clear:both"></div>
              </div>

            </div>
            <div class="col-xs-12" style="display: none;">
              {if $conditions}
              <p>{$conditions}</p>
              {/if}
              {if $msg}
              <p class="alert {if $nw_error}alert-danger{else}alert-success{/if}">
                {$msg}
              </p>
              {/if}
              {if isset($id_module)}
              {hook h='displayGDPRConsent' id_module=$id_module}
              {/if}
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>